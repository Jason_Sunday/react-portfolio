export const Skills = [
    {
        name: "CSS",
        rank: 80
    },
    {
        name: "HTML",
        rank: 90
    },
    {
        name: "JavaScript",
        rank: 80
    },
    {
        name: "TypeScript",
        rank: 80
    },
    {
        name: "Angular",
        rank: 80
    },
    {
        name: "React",
        rank: 70
    },
    {
        name: "C#",
        rank: 60
    },
    {
        name: "SQL",
        rank: 40
    },
    {
        name: "UX/UI Design",
        rank: 70
    },
    {
        name: "Photoshop",
        rank: 85
    },
    {
        name: "Restful API's",
        rank: 60
    }
];