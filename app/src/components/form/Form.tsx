import React from 'react';
import Iframe from 'react-iframe'
function Form() {

    const style = {"min-width": "100%", height: "100%", width: "100%", border: "none"};
    return (
        <Iframe url="https://form.jotform.com/212546461098156"
            id="JotFormIFrame-212546461098156"
            title="Information Request Form"
            allow="geolocation; microphone; camera"
            src="https://form.jotform.com/212546461098156" 
            height="100%"
            width="100%"
            frameBorder={0}
            scrolling="no" />
    );
}

export default Form
