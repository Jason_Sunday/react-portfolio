import React from 'react'
import classes from "./Nav.module.scss";

function Nav() {
    return (
        <nav className={classes.navbar}>
            <div className="maximum-width-container">
                {/* Brand  */}
                <div className={classes.navFlexContainer}>
                    <a className={classes.navbarBrand} href="#">
                        Jason Sunday
                    </a>
                    <ul className={classes.navbarNav}>
                        <li className={classes.navItem}>
                            <a className={classes.navLink} href="#about">ABOUT</a>
                        </li>
                        {/* <li className={classes.navItem}>
                            <a className={classes.navLink} href="#portfolio">PORTFOLIO</a>
                        </li> */}
                        <li className={classes.navItem}>
                            <a className={classes.navLink} href="#contact">CONTACT</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Nav
