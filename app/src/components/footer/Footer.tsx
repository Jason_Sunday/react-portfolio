import React from 'react';
import classes from './Footer.module.scss';
import facebook from "../../assets/facebook.png" ;
import linkedIn from "../../assets/linkedin.png" ;
import email from "../../assets/resume.png";

function Footer() {
    return (
        <div className={classes.footer}>
            <div className="maximum-width-container">
                <div className={classes.footerGrid}>
                    <div className="left">
                        <div className={classes.footContainer}>
                            <a href="https://www.linkedin.com/in/jasonsunday" target="_blank">
                                <img src={linkedIn} alt="LinkedIn Icon" className={classes.social} />
                            </a>
                            <a href="https://www.facebook.com/mjasonsunday" target="_blank">
                                <img src={facebook} alt="Facebook Icon" className={classes.social} />
                            </a>
                            {/* <a href="" target="_blank">
                                <img src={email} alt="Resume Icon" className={classes.social} />
                            </a> */}
                        </div>
                    </div>
                    <div className="right">
                        <div className={classes.copy}>
                            Jason Sunday
                            <span>©2021</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Footer
