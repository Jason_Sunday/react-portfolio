import React from 'react'
import classes from "./ProgressBar.module.scss";

interface ProgressBarProps {
    skill: string;
    rank: number;
}

function ProgressBar({skill, rank}: ProgressBarProps) {
    const currentProgress = {
        width: `${rank}%`
    };
    return (
        <div className={classes.progress}>
            <div className={classes.progressBar} role="progressbar" style={currentProgress}>
                <div className={classes.skillData}>
                    <div className={classes.dataItem}>{skill}</div>
                    <div className={classes.dataItem}>{rank}%</div>
                </div>
            </div>
        </div>
    );
}

export default ProgressBar
