import React, { useState } from "react";
import classes from "./App.module.scss";
import Footer from "./components/footer/Footer";
import Form from "./components/form/Form";
import Nav from "./components/nav/Nav";
import ProgressBar from "./components/progressbar/ProgressBar";
import { Skills } from "./data/data";
import myPhoto from "../src/assets/jason-sunday.jpg";

function App() {
  const [skills, setSkills] = useState(Skills);

  function renderSkills() {

    return skills.map(skill => {
      return <ProgressBar skill={skill.name} rank={skill.rank} key={skill.name} />;
    });
  }

  return (
    <>
      <Nav />
      <div className={classes.jumbotron}>
        <div className="maximum-width-container">
          <div className={classes.jumboText}>
            <h1>Hi! I'm Jason Sunday.</h1>
            <h2>And I'm a Web Designer and full-stack Web Developer.</h2>
            <a href="#about">
              <button type="button" className={classes.aboutMeBtn}>
                About Me
              </button>
            </a>
          </div>
        </div>
      </div>

      {/* About Me */}
      <div className="maximum-width-container section-margin" id="about">
        <h1>About</h1>
        <div className={classes.aboutContainer}>
          <div className="left">
            <img
              src={myPhoto}
              alt="Jason Sunday Web Developer Web Designer"
              className="me"
            />
            <h3>Who am I?</h3>
            <p>
              I am a Web Designer and Developer. I am currently working for
              Priority Dispatch as a Lead Software Developer in Salt Lake City, UT. I love creating
              beautifully designed websites that meet my clients needs and the
              needs of their customers.
            </p>
          </div>
          <div className="right">
            <div className={classes.skillsContainer}>
              {renderSkills()}
            </div>
          </div>
        </div>
      </div>

      {/* Contact */}
      <div id="contact" className="maximum-width-container section-margin">
        <h1>Contact</h1>
        <div className={classes.contactContainer}>
          <Form/>
        </div>
      </div>
      {/* Foot */}
      <div className="section-margin">
        <Footer/>
      </div>
    </>
  );
}

export default App
